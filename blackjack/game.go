package blackjack

import (
	"fmt"
	"gitlab.com/jwambugu/gophercises_blackjack_ai/deck"
)

type State int8

const (
	statePlayerTurn State = iota
	stateDealerTurn
	StateHandOver
)

// Game
type Game struct {
	// unexported fields
	deck     []deck.Card
	state    State
	player   []deck.Card
	dealer   []deck.Card
	dealerAI AI
	balance  int
}

// Move
type Move func(*Game)

// New creates a new Game
func New() Game {
	return Game{
		state:    statePlayerTurn,
		dealerAI: dealerAI{},
		balance:  0,
	}
}

// drawCards removes the first card in the deck, return the card and all
// the other cards without the drawn card
func drawCards(cards []deck.Card) (deck.Card, []deck.Card) {
	return cards[0], cards[1:]
}

func (g *Game) currentHand() *[]deck.Card {
	switch g.state {
	case statePlayerTurn:
		return &g.player
	case stateDealerTurn:
		return &g.dealer
	default:
		panic("it isn't currently any player's turn")
	}
}

// MoveHit
func MoveHit(g *Game) {
	hand := g.currentHand()

	var card deck.Card

	card, g.deck = drawCards(g.deck)
	*hand = append(*hand, card)

	if Score(*hand...) > 21 {
		MoveStand(g)
	}
}

// MoveStand
func MoveStand(g *Game) {
	g.state++
}

func deal(g *Game) {
	g.player = make([]deck.Card, 0, 5)
	g.dealer = make([]deck.Card, 0, 5)

	var card deck.Card

	for i := 0; i < 2; i++ {
		card, g.deck = drawCards(g.deck)
		g.player = append(g.player, card)

		card, g.deck = drawCards(g.deck)
		g.dealer = append(g.dealer, card)
	}

	g.state = statePlayerTurn
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func minimumScore(hand ...deck.Card) int {
	score := 0

	for _, card := range hand {
		score += min(int(card.Rank), 10)
	}

	return score
}

// Score will take in a hand of cards and return the best blackjack score possible with the hand
func Score(hand ...deck.Card) int {
	minScore := minimumScore(hand...)

	if minScore > 11 {
		return minScore
	}

	for _, card := range hand {
		if card.Rank == deck.Ace {
			// ace is currently worth 1, we'll change it to 11
			// 11 - 1 = 10
			return minScore + 10
		}
	}

	return minScore
}

// SoftScore returns true if an Ace is being counted as 11 points
func SoftScore(hand ...deck.Card) bool {
	minScore := minimumScore(hand...)
	score := Score(hand...)

	return minScore != score
}

func endHand(g *Game, ai AI) {
	playerScore, dealerScore := Score(g.player...), Score(g.dealer...)

	switch {
	case playerScore > 21:
		fmt.Println("You busted!!")
		g.balance--
	case dealerScore > 21:
		fmt.Println("Dealer busted!!")
		g.balance++
	case playerScore > dealerScore:
		fmt.Println("You win!!")
		g.balance++
	case dealerScore > playerScore:
		fmt.Println("You lose!!")
		g.balance--
	case dealerScore == playerScore:
		fmt.Println("You draw!!")
	}

	fmt.Println()

	ai.Results([][]deck.Card{g.player}, g.dealer)
	g.player = nil
	g.dealer = nil
}

func (g *Game) Play(ai AI) int {
	g.deck = deck.New(deck.Deck(3), deck.Shuffle)
	for i := 0; i < 2; i++ {
		deal(g)

		for g.state == statePlayerTurn {
			hand := make([]deck.Card, len(g.player))

			copy(hand, g.player)

			move := ai.Play(hand, g.player[0])
			move(g)
		}

		for g.state == stateDealerTurn {
			hand := make([]deck.Card, len(g.dealer))

			copy(hand, g.dealer)

			move := g.dealerAI.Play(hand, g.dealer[0])
			move(g)
		}

		endHand(g, ai)
	}

	return g.balance
}
