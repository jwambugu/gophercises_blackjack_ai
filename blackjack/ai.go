package blackjack

import (
	"fmt"
	"gitlab.com/jwambugu/gophercises_blackjack_ai/deck"
)

type humanAI struct{}

func HumanAI() AI {
	return humanAI{}
}

type dealerAI struct{}

// AI
type AI interface {
	Play(hand []deck.Card, dealer deck.Card) Move
	Results(hand [][]deck.Card, dealer []deck.Card)
	Bet() int
}

// Play
func (ai humanAI) Play(hand []deck.Card, dealer deck.Card) Move {
	for {
		fmt.Println("Player: ", hand)
		fmt.Println("Dealer: ", dealer)
		fmt.Println("What will you do? (h)it or (s)tand?")

		var userInput string

		_, _ = fmt.Scanf("%s\n", &userInput)

		switch userInput {
		case "h":
			return MoveHit
		case "s":
			return MoveStand
		default:
			fmt.Println("Invalid option: ", userInput)
		}
	}
}

// Bet
func (ai humanAI) Bet() int {
	return 1
}

// Results prints out the final result of the game for the human
func (ai humanAI) Results(hand [][]deck.Card, dealer []deck.Card) {
	fmt.Println("== FINAL HANDS ==")
	fmt.Println("Player:", hand)
	fmt.Println("Dealer:", dealer)
}

// Bet
func (ai dealerAI) Bet() int {
	return 1
}

// Play returns a Move made by the dealer ai
func (ai dealerAI) Play(hand []deck.Card, dealer deck.Card) Move {
	// If dealer score <= 16, hit
	// If dealer has a soft 17, hit
	dealerScore := Score(hand...)

	if dealerScore <= 16 || (dealerScore == 17 && SoftScore(hand...)) {
		return MoveHit
	}
	return MoveStand
}

// Results prints out the final result of the game for the AI
func (ai dealerAI) Results(hand [][]deck.Card, dealer []deck.Card) {
	fmt.Println("== FINAL HANDS ==")
	fmt.Println("Player:", hand)
	fmt.Println("Dealer:", dealer)
}
