package main

import (
	"fmt"
	"gitlab.com/jwambugu/gophercises_blackjack_ai/blackjack"
)

func main() {
	game := blackjack.New()
	winnings := game.Play(blackjack.HumanAI())

	fmt.Println(winnings)
}
