//go:generate stringer -type=Suit,Rank

package deck

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

// Suit is one of the categories into which the cards of a deck are divided
type Suit uint8

const (
	Spade Suit = iota
	Diamond
	Club
	Heart
	Joker
)

// Rank is the number on the card.
type Rank uint8

const (
	_ Rank = iota
	Ace
	Two
	Three
	Four
	Five
	Six
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
)

// Card is a collection of Suit and Rank
type Card struct {
	Suit
	Rank
}

var suits = [...]Suit{Spade, Diamond, Club, Heart}

const (
	minimumRank = Ace
	maximumRank = King
)

type less func(cards []Card) func(i, j int) bool

var randomShuffler = rand.New(rand.NewSource(time.Now().Unix()))

// String will return a string representation of the Card.
// Example, Ace of Hearts
func (c Card) String() string {
	if c.Suit == Joker {
		return c.Suit.String()
	}

	return fmt.Sprintf("%s of %ss", c.Rank.String(), c.Suit.String())
}

// absoluteRank will return the Card rank in the deck
func absoluteRank(c Card) int {
	return int(c.Suit)*int(maximumRank) + int(c.Rank)
}

// Less will return true if the absolute rank of Card i is less than absolute Rank of Card j
// Otherwise returns false
func Less(cards []Card) func(i, j int) bool {
	return func(i, j int) bool {
		return absoluteRank(cards[i]) < absoluteRank(cards[j])
	}
}

// DefaultSort sorts the cards in the order of smallest to largest Card
func DefaultSort(cards []Card) []Card {
	sort.Slice(cards, Less(cards))
	return cards
}

// Sort allows a custom sorting function to be used for sorting the cards.
func Sort(less less) func([]Card) []Card {
	return func(cards []Card) []Card {
		sort.Slice(cards, less(cards))
		return cards
	}
}

// Shuffle returns a new deck of cards which have been shuffled.
func Shuffle(cards []Card) []Card {
	shuffledCards := make([]Card, len(cards))

	for i, j := range randomShuffler.Perm(len(cards)) {
		shuffledCards[i] = cards[j]
	}

	return shuffledCards
}

// Jokers adds n number of Jokers to the Deck
// The added Joker gets a Rank of the loop iteration
func Jokers(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		for i := 0; i < n; i++ {
			cards = append(cards, Card{
				Rank: Rank(i),
				Suit: Joker,
			})
		}

		return cards
	}
}

// Filter creates a new deck of Cards. It removes a card from the deck
func Filter(fn func(card Card) bool) func([]Card) []Card {
	return func(cards []Card) []Card {
		var newDeck []Card

		for _, c := range cards {
			if !fn(c) {
				newDeck = append(newDeck, c)
			}
		}

		return newDeck
	}
}

// Deck creates n number of decks
func Deck(n int) func([]Card) []Card {
	return func(cards []Card) []Card {
		var newDeck []Card

		for i := 0; i < n; i++ {
			newDeck = append(newDeck, cards...)
		}

		return newDeck
	}
}

// New will create a new deck of Card
func New(opts ...func([]Card) []Card) []Card {
	var cards []Card

	for _, suit := range suits {
		for rank := minimumRank; rank <= maximumRank; rank++ {
			cards = append(cards, Card{
				Suit: suit,
				Rank: rank,
			})
		}
	}

	for _, opt := range opts {
		cards = opt(cards)
	}

	return cards
}
