package deck

import (
	"fmt"
	"math/rand"
	"testing"
)

func ExampleCard() {
	fmt.Println(Card{Rank: Ace, Suit: Heart})
	fmt.Println(Card{Rank: Two, Suit: Spade})
	fmt.Println(Card{Rank: Nine, Suit: Diamond})
	fmt.Println(Card{Rank: Jack, Suit: Club})
	fmt.Println(Card{Suit: Joker})

	// Output:
	// Ace of Hearts
	// Two of Spades
	// Nine of Diamonds
	// Jack of Clubs
	// Joker
}

func TestNew(t *testing.T) {
	cards := New()

	// Deck has 52 cards, 13 ranks * 4 suits
	if len(cards) != 52 {
		t.Error("Wrong number of cards in the new deck.")
	}

	firstCard := Card{Rank: Ace, Suit: Spade}

	if cards[0] != firstCard {
		t.Errorf("Expected Ace of Spades as first card, got %s", cards[0].String())
	}
}

func TestDefaultSort(t *testing.T) {
	cards := New(DefaultSort)

	firstCard := Card{Rank: Ace, Suit: Spade}

	if cards[0] != firstCard {
		t.Errorf("Expected %s as first card, got %s", firstCard, cards[0])
	}
}

func TestSort(t *testing.T) {
	cards := New(Sort(Less))

	firstCard := Card{Rank: Ace, Suit: Spade}

	if cards[0] != firstCard {

	}
}

func TestShuffle(t *testing.T) {
	// Makes randomShuffler deterministic.
	// First call to randomShuffler.Perm(52) should be
	// [40 35 ...]
	randomShuffler = rand.New(rand.NewSource(0))

	originalDeck := New()
	firstCard := originalDeck[40]
	secondCard := originalDeck[35]

	shuffledCards := New(Shuffle)

	if shuffledCards[0] != firstCard {
		t.Errorf("Expected first card to be %s, received %s", firstCard, shuffledCards[0])
	}

	if shuffledCards[1] != secondCard {
		t.Errorf("Expected first card to be %s, received %s", secondCard, shuffledCards[1])
	}
}

func TestJokers(t *testing.T) {
	cards := New(Jokers(3))
	count := 0

	for _, c := range cards {
		if c.Suit == Joker {
			count++
		}
	}

	if count != 3 {
		t.Errorf("Expected 3 Jokers, received %d", count)
	}
}

func TestFilter(t *testing.T) {
	filter := func(card Card) bool {
		return card.Rank == Two || card.Rank == Three
	}

	cards := New(Filter(filter))

	for _, card := range cards {
		if card.Rank == Two || card.Rank == Three {
			t.Error("Expected all twos and threes to be filtered out")
		}
	}
}

func TestDeck(t *testing.T) {
	cards := New(Deck(3))

	expectedCards := 13 * 4 * 3

	// 13 ranks * 4 suits * 3 decks
	if len(cards) != expectedCards {
		t.Errorf("Expected %d cards, received %d", expectedCards, len(cards))
	}

}
